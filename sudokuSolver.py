import os, csv
import numpy as np
from copy import deepcopy

test = '004300209005009001070060043006002087190007400050083000600000105003508690042910300'

def printBoardRow(row, spaces):
    for i in range(9):
        print(''.join(row[i]) + ' '*(1 + spaces - len(row[i])), end='') # Prints the item with spacing

        if i in (2, 5):
            print('| ', end='')
    print()

def printBoard(board):
    maxLength = 0
    # Getting the longest item in the board:
    for row in board:
        for space in row:
            if len(space) > maxLength:
                maxLength = len(space)

    for i in range(9):
        printBoardRow(board[i], maxLength)
        if i in (2, 5):
            print('-' * (9*maxLength + 12))

def initFromString(string):
    # Initializes single probabilities for known numbers
    board = [[[] for i in range(9)] for j in range(9)]
    for i in range(len(board)):
        for j in range(len(board[0])):
            board[i][j] = [string[9*i+j]]

    # Initializing empty squares as 1-9 arrays
    for i in range(len(board)):
        for j in range(len(board[0])):
            if board[i][j] == ['0']:
                board[i][j] = [str(k) for k in range(1, 10)]
    return board

def tryReduce(board):
    for i in range(9):
        for j in range(9):
            if len(board[i][j]) == 1:
                number = board[i][j][0]
                # First pass: first index elimination
                for k in range(9):
                    if board[k][j] == board[i][j]:
                        continue
                    try:
                        board[k][j].remove(number)
                    except ValueError:
                        None
                # Second pass: second index elimination
                for k in range(9):
                    if board[i][k] == board[i][j]:
                        continue
                    try:
                        board[i][k].remove(number)
                    except ValueError:
                        None
                # Third pass: 3x3 grid elimination
                
                #Snapping the coordinate to the 3x3 grid
                gridIndices = (int(i/3)*3, int(j/3)*3)
                # print(gridIndices)
                # Scanning over the other boxes 
                for m in range(3):
                    for n in range(3):
                        index1 = gridIndices[0]+m
                        index2 = gridIndices[1]+n
                        if index1 == i and index2 == j:
                            continue
                        try:
                            board[index1][index2].remove(number)
                            # print((index1, index2))
                        except ValueError:
                            None
    return board

board = initFromString(test)

iteration = 0
oldBoard = deepcopy(board)
while oldBoard != board or iteration == 0:
    print('Iteration {}:'.format(iteration))
    printBoard(board)
    oldBoard = deepcopy(board)
    board = tryReduce(board)
    iteration += 1

print('Done!')